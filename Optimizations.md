# Prime Numbers Algorithms - some ideas for optimization

## Caching and divide and conquer

Caching the primes already calculated along with using a divide and conquer strategy using recursion can be used in speeding up the algorithm.

While recursing, when a number for which we already have cached primes is encountered , the cached values can be used instead of repeating the calculation.

## Optimizing with standard rules for Identifying prime numbers

Some simple rules like :
Numbers ending with 0, 2, 4, 6 and 8 are never prime numbers

Can be used to skip some of the calculations in the algorithm.

## Multithreading

The problem can be broken down into smaller chunks and each can be processed in a separate thread and result joined in the end.
