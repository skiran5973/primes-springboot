/**
* Sriram Satyavolu  © Threerhinos LLC.
**/
package com.hexagon.test.primes.app;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.hexagon.test.primes.services.EratosthenesSieve;

@SpringBootTest()
class PrimesApplicationTests {
	
	Logger logger = LoggerFactory.getLogger(PrimesApplicationTests.class);

	@Autowired
	EratosthenesSieve eratosthenesSieve;
	
	@Test
	void testPrimes() {
		List<Integer> primes = eratosthenesSieve.getPrimes(100);
		int[] extected = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};
		logger.info(primes.toString());
		assertTrue(primes.equals(Arrays.asList(extected)));
		
		primes = eratosthenesSieve.getPrimes(2);
		int[] extected1 = {2};
		logger.info(primes.toString());
		assertTrue(primes.equals(Arrays.asList(extected1)));

		primes = eratosthenesSieve.getPrimes(8);
		int[] extected2 = {2,3,5,7};
		logger.info(primes.toString());
		assertTrue(primes.equals(Arrays.asList(extected2)));

	}

}
