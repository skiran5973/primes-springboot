/**
* Sriram Satyavolu  © Threerhinos LLC.
**/
package com.hexagon.test.primes.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hexagon.test.primes.services.EratosthenesSieve;

@RestController
public class PrimeFinder {
	@Autowired
	EratosthenesSieve primesFilder;
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(path="/primes")
	private Primes getPrimesLessThan(@RequestParam int n){
		Primes primes;
		if (n>1) {
			primes = new Primes(n , this.primesFilder.getPrimes(n));	
		}else {
			primes = new Primes(1,null);
		}
		return primes;
		
		
	}
}
