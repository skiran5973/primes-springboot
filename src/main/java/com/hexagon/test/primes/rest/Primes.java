/**
* Sriram Satyavolu  © Threerhinos LLC.
**/
package com.hexagon.test.primes.rest;

import java.util.List;

public class Primes {
	
	int range;
	List<Integer >primes;
	
	public Primes(int n, List<Integer> primes) {
		this.range = n;
		this.primes = primes;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	public List<Integer > getPrimes() {
		return this.primes;
	}
	public void setPrimes(List<Integer > primes) {
		this.primes = primes;
	}
	
	

}
