/**
* Sriram Satyavolu  © Threerhinos LLC.
**/
package com.hexagon.test.primes.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * To find all the prime numbers less than or equal to a given integer n by Eratosthenes' method
 * @author sriramsatyavolu © Threerhinos LLC.
 *
 */
@Component
public class EratosthenesSieve {
	Logger logger = LoggerFactory.getLogger(EratosthenesSieve.class);
	List<Integer> numberList;	
	
	public List<Integer> getPrimes(int n ) {
		return calculatePrimes(n ); 
		
	}
	private List<Integer> calculatePrimes(int n ){
		
		//Create a list of consecutive integers from 2 through n: (2, 3, 4, ..., n).
		//Initially, let p equal 2, the smallest prime number.
		this.numberList = new ArrayList<>(n-2);
		for (int i = 0 ; i <= n-2;i++ ) {
			this.numberList.add(i+2);
		}
		
		//Enumerate the multiples of p by counting in increments of p from 2p to n, and mark them in the list (these will be 2p, 3p, 4p, ...; the p itself should not be marked).
		for (int i = 2; i <= n;i++) {
			int k=2;
			for (int j = i ; j <= n ; j++) {
				int nextComposite = j*k;
				k++;
				// is there a way to check if nextComposite is already 0?
				if (nextComposite  <= n) {
					//set number to 0 to indicate it is not prime.
					this.numberList.set(nextComposite-2, 0);
				}else {
					break;
				}
			}
		}
		
		//Find the smallest number in the list greater than p that is not marked. If there was no such number, stop. Otherwise, let p now equal this new number (which is the next prime), and repeat from step 3.
		//When the algorithm terminates, the numbers remaining not marked in the list are all the primes below n.
		return this.numberList.stream().filter(s-> s!=0).collect(Collectors.toCollection(ArrayList<Integer>::new));
	}
	@Override
	public String toString() {
		return "EratosthenesSieve [numberList=" + numberList + "]";
	}
	
	

}
